#Tee ohjelma, joka kysyy käyttäjältä “Millä alkukirjaimella etsitään?”. Ohjelma tulostaa allekkain listasta ne sukunimet, 
#jotka alkavat käyttäjän antamalla kirjaimella.
sukunimet = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]

snimi = input("Millä alkukirjaimella etsitään? ")
for sukunimi in sukunimet:
    if sukunimi[0] == snimi:
        print(sukunimi)