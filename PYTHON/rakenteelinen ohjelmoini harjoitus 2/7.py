#Tee ohjelma, joka kysyy käyttäjältä “Monesko nimi tulostetaan?”. Sitten ohjelma tulostaa listasta sen sukunimen, joka on järjestyksessä se käyttäjän antama järjestysluku.

sukunimet = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]

monesko = int(input("Monesko nimi tulostetaan? "))

if 1 <= monesko <= len(sukunimet):
    print(sukunimet[monesko - 1])