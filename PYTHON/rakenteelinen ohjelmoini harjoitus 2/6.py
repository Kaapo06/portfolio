#Tee ohjelma, joka kysyy käyttäjältä ensin “Mitä kirjainta etsitään?”. Sitten ohjelma kysyy "Monesko merkki sukunimessä?". 
#Ohjelma tulostaa allekkain ne sukunimet, joissa on käyttäjän antama kirjain siinä kohdassa minkä käyttäjä antoi.

sukunimet = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
snimi = []
snimi1 = input("Millä alkukirjaimella etsitään? ")
merkki = int(input("Monesko merkki sukunimessä? "))
for sukunimi in sukunimet:
    if sukunimi[merkki] == snimi1:
        snimi.append(sukunimi)
    
for sukunimi in snimi:
        print(sukunimi)