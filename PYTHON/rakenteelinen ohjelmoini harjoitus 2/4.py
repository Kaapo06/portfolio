#Tee ohjelma, joka kopioi listasta uuteen listaan ne sukunimet, 
#jotka päättyvät s-kirjaimeen. Sen jälkeen ohjelma tulostaa uuden listan alkiot aakkosjärjestyksessä allekkain.
sukunimet = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
snimi = []
for sukunimi in sukunimet:
    if sukunimi[-1] == "s":
        snimi.append(sukunimi)
    
        snimi.sort()
for sukunimi in snimi:
        print(sukunimi)