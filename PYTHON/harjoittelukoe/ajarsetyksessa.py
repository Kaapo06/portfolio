#Tee ohjelma, joka pyytää käyttäjältä nimiä ja tallentaa ne listaan. Lopuksi listassa olevat nimet tulostetaan alekkain aakkosjärjestyksessä. 
nimet = []

while True:
    nimi = input("Kerro nimiä: ")
    if nimi == "":
        break
    nimet.append(nimi)

nimet.sort()

for nimi in nimet:
    print(nimi)