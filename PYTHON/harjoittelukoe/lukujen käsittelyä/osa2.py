#Laajenna ohjelmaa siten, että se tulostaa syötettyjen lukujen summa. Syötteen loppumisesta kertovaa nollaa ei tule ottaa huomioon summan laskemisessa. 
#Ohjelman tulostus laajenee seuraavasti: 

mr = 0
s = 0

while True:
    num = int(input("Anna luku: "))
    if num == 0:
        break
    mr += 1
    s += num
print(f"Kuinka monta numeroa annoit {mr}")
print(f"Lukujen summa on {s}")
