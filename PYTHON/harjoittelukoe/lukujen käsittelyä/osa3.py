#Laajenna ohjelmaa siten, että se tulostaa syötettyjen lukujen keskiarvon. 
#Syötteen loppumisesta kertovaa nollaa ei tule ottaa huomioon keskiarvon laskemisessa. Voit olettaa, että käyttäjä syöttää aina vähintään yhden luvun.

mr = 0
s = 0

while True:
    num = int(input("Anna luku: "))
  
    if num == 0:
        break
    mr += 1
    s += num
    s / mr
print(f"Kuinka monta numeroa annoit {mr}")
print(f"Lukujen summa on {s}")
print(f"Lukujen keskiarvo on {mr}")