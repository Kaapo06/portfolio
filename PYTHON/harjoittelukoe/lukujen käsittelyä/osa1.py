#Tee ohjelma, joka pyytää käyttäjää syöttämään kokonaislukuja. Ohjelma pyytää lukuja niin kauan kunnes käyttäjä syöttää nollan. 
#Syötteiden lukemisen jälkeen ohjelman tulee tulostaa syötettyjen lukujen lukumäärä. Syötteen loppumisesta kertovaa nollaa ei tule ottaa huomioon lukumäärässä. 
#Tarvitset tässä uuden muuttujan, jonka avulla pidät kirjaa luettujen lukujen määrästä. 
mr = 0

while True:
    num = int(input("Anna luku: "))
    
    if num == 0:
        break
    mr += 1
print(f"Kuinka monta numeroa annoit {mr}")




