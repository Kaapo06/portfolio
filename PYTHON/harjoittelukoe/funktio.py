#Tee funktio keskiarvo, joka saa parametrina kolme kokonaislukua. Funktio tulostaa parametriensa keskiarvon. 
def keskiarvo(x, y, z):
    määrä = 3
    tulos = x + y + z
    print(f"{tulos / määrä}")

if __name__ == "__main__":
    keskiarvo(1, 2, 3)