#1
''' 
ika = int(input("Syötä luku: "))

if ika >= 18:
    print("Olet täysi-ikäinen")
else:
    print("Olet alaikäinen")
'''

#2
'''
luku = int(input("Syötä luku: "))

if luku > 50:
    print("Luku on suurempi kuin 50")

if luku < 50:
    print("Luku on pienempi kuin 50")
'''

#3
'''
ika = int(input("Anna ikasi "))
if ika >= 18 and ika <= 80:
    print("Pääset klubille")
else:
    print("ET pääse ")
'''

#4
'''
while True:
    pituus = int(input("Pituus: "))
    if pituus > 0:
        break

    print("Ei kelpaa")

while True:
    hinta = float(input("Hinta: "))
    if hinta > 0:
        break

    print("Ei kelpaa")

suurempi = pituus * 1.25 * hinta
pienempi = pituus * 1.50 * hinta

if pituus > 50:
    print(f"Hinnaksi tulee = {suurempi}")

if pituus <= 50:
    print(f"Hinnaksi tulee = {pienempi}")
'''