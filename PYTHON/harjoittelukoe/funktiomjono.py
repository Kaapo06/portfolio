#Tee funktio tulosta_monesti(merkkijono, kertaa), joka saa parametriksi merkkijonon sekä kokonaisluvun, joka kertoo, montako kertaa funktion tulee tulostaa parametrina saamansa merkkijono 
def tulosta_monesti(merkkijono, kertaa):
    while kertaa > 0:
        print(merkkijono)
        kertaa -= 1

tulosta_monesti("Moikkeliskoikkelis", 3)