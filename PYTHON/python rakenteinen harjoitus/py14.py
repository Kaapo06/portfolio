print("Anna kokonaislukuja, niin lasken niiden summan (nolla lopettaa):")
s = 0
while True:
    n1 = int(input(""))
    
    if n1 == 0:
        break
    s += n1

print(f"Lukujen summa on {s}")