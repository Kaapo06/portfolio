sukunimet = ["Pekkala", "Suominen", "Pitkämäki", "Männikkö", "Tapanila", "Mäki"]
haluttu_alkukirjain = input("Millä alkukirjaimella etsitään: ")

loytyneet_nimet = [nimi for nimi in sukunimet if nimi.startswith(haluttu_alkukirjain)]

for nimi in loytyneet_nimet:
        print(nimi)