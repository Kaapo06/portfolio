console.log("Joo tää varmaa toimii"); //testataan toimiiko ohjelma
//määritellään muuttuja, vakio, pysyy aina samana
//hae html documentista canvas
const canvas = document.getElementById('canvaasi');
//tehdään 2d juttuja
const ctx = canvas.getContext('2d');
// Let myös määrittelee muuttujan, mutta sen arvoa voi muuttaa, var
let raf;
let inputStates = {}
window.addEventListener('keydown', function(event){
console.log(event.key);
if (event.key == "ArrowRight") {
  console.log("Oikea nuoli painettu");
  inputStates.right = true;
}

if (event.key == "ArrowLeft") {
  console.log("Vasen nuoli painettu");
  inputStates.left = true;
}
}, false);

window.addEventListener('keyup', function(event){
  console.log(event);
  if (event.key == "ArrowRight") {
    console.log("Oikea nuoli nostettu");
    inputStates.right = false;
  }
  
  if (event.key == "ArrowLeft") {
    console.log("Vasen nuoli nostettu");
    inputStates.left = false;
  }
  }, false);

//javascriptin objekti määrittely, ominaisuuksien avoja voi muuttaa
const ball = {
  x: 50,
  y: 50,
  vx: 5,  //nopeus ja suunta
  vy: 2,
  radius: 15, //pallon säde
  color: 'white', //väri
  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }
};  //tähän loppuu pallon määrittely
const maila = {
  x: 250,
  y: 385,
  vx: 5,  //nopeus ja suunta
  vy: 2,
leveys: 100,
korkeus: 15,
  color: 'white',
  draw() {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x,this.y, this.leveys, this.korkeus);


    ctx.fill();
  }
};  //tähän loppuu mailan määrittely


//peliluuppi
//functio on koodinpätkä, joka suoritetaan vain käskystä
function draw() {

  //tyhjennä kaikki
  ctx.clearRect(0,0, canvas.width, canvas.height);

  //piirrä pelimaailma
ball.draw();
maila.draw();

  //liikutetaan pelimaailmaa
  ball.x += ball.vx;
  ball.y += ball.vy;

// liikuta mailaa
 if (inputStates.right){
 maila.x = maila.x + maila.vx;
 }

 if (inputStates.left) {
  maila.x = maila.x - maila.vx;
 }

//reunat
  if (ball.y + ball.radius > canvas.height || ball.y - ball.radius < 0) {
    ball.vy = -ball.vy;
  }
  if (ball.x + ball.radius > canvas.width || ball.x - ball.radius < 0) {
    ball.vx = -ball.vx;
  }


//mailan seiniin pysäytys
if (maila.x < 0){
  maila.x = 0; 
}
if (maila.x + maila.leveys > canvas.width) {
  maila.x = canvas.width - maila.leveys;
}


//törmääkö pallo pailaan
var testX = ball.x;
var testY = ball.y;

if (testX < maila.x) testX = maila.x;
else if (testX >(maila.x + maila.leveys)) testX = (maila.x + maila.leveys) ;
if (testY < maila.y) testY = maila.y;
else if (testY > (maila.y + maila.korkeus)) testY = (maila.y + maila.korkeus);

var distX = ball.x - testX;
var distY = ball.y - testY;
var dista = Math.sqrt((distX * distX) + (distY * distY));

if (dista <= ball.radius) {
  if (ball.x >= maila.x && ball.x <= (maila.x + maila.leveys)){
    ball.vy *= -1;
  }
  else if (ball.y >= maila.y && ball.y <= (maila.y + maila.korkeus)){
    ball.vx *= -1;
  }
}
//odota
  raf = window.requestAnimationFrame(draw);
}     
//funktion määritys loppuu tähän

draw(); //suorita tämä funktio


